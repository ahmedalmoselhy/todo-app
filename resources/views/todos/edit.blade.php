@extends('layouts.app')

@section('content')
<h1 class="text-center">
    Create Todos
</h1>
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card card-default">
            <div class="card-header">Edit Todo</div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="list-group">
                            @foreach ($errors->all() as $error)
                                <li class="list-group-item">
                                    {{ $error }}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="/todos/{{ $todo-> id }}/update-todo" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="name">Todo Name</label>
                        <input type="text" class="form-control" name="name" value="{{ $todo->name }}">
                    </div>
                    <div class="form-group">
                        <label for="content">Content</label>
                        <textarea name="description" id="" cols="30" rows="10" class="form-control">{{ $todo->description }}</textarea>
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-success" type="submit">Update Todo</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
